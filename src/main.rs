use std::sync::{Arc, Mutex};
use std::io::{self, Write, stdin};
use std::thread;
use std::time::Duration;
use ev3dev_lang_rust::sensors::{UltrasonicSensor, SensorPort, TouchSensor};
use tui::{
    *,
    layout::*,
    backend::TermionBackend,
    style::{Color, Style},
    widgets::*,
};
use termion::{
    raw::IntoRawMode,
    clear,
    event::Key,
    input::TermRead,
};

#[derive(Clone)]
struct Touch(Arc<Mutex<bool>>);

impl Touch {
    fn new() -> Self {
        Touch(Arc::new(Mutex::new(true)))
    }

    fn update(&mut self, update: bool) {
        let mut value = self.0.lock().unwrap();
        *value = update;
    }

    fn get(&self) -> bool {
        let value = self.0.lock().unwrap();
        *value
    }
}

#[derive(Clone)]
struct Distance(Arc<Mutex<f32>>);

impl Distance {
    fn new() -> Self {
        Distance(Arc::new(Mutex::new(0.0)))
    }

    fn update(&mut self, update: f32) {
        let mut new_value = update;
        let mut value = self.0.lock().unwrap();
        if new_value <= 0.0 {
            new_value = 0.0;
        };
        if new_value >= 250.0 {
            new_value = 250.0;
        };
        *value = new_value;
    }

    fn get(&self) -> f32 {
        let value = self.0.lock().unwrap();
        *value
    }
}

fn main() -> Result<(), io::Error> {
    let mut stdout = io::stdout().into_raw_mode()?;
    write!(stdout, "{}", clear::All)?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    let bar = Distance::new();
    let mut bar1 = bar.clone();
    thread::spawn(move || {
        let ultrasonic = UltrasonicSensor::get(SensorPort::In4).unwrap();
        ultrasonic.set_mode_us_dc_cm().unwrap();
        loop {
            bar1.update(ultrasonic.get_distance_centimeters().unwrap());
            thread::sleep(Duration::from_millis(10));
        }
    }
    );

    let touch = Touch::new();
    let mut touch1 = touch.clone();
    thread::spawn(move || {
        let touchsensor = TouchSensor::find().unwrap();
        loop {
            touch1.update(touchsensor.get_pressed_state().unwrap());
            thread::sleep(Duration::from_millis(10));
        }
    });

    thread::spawn(move ||
        loop {
            let value = bar.get();
            terminal.draw(|f| {
                let vertical = Layout::default()
                    .direction(Direction::Vertical)
                    .margin(1)
                    .constraints(
                        [
                            Constraint::Percentage(20),
                            Constraint::Percentage(20),
                            Constraint::Percentage(60),
                        ]
                            .as_ref(),
                    )
                    .split(f.size());
                let label = format!("{:.2}", value);
                let gauge = Gauge::default()
                    .block(Block::default().title(" UltraSonic ").borders(Borders::ALL))
                    .gauge_style(Style::default().fg(Color::LightRed))
                    .percent((value / 2.5).round() as u16).label(label);
                let color = match touch.get() {
                    true => Color::Green,
                    false => Color::Red,

                };
                let button = Gauge::default()
                    .block(Block::default().title(" Touch ").borders(Borders::ALL))
                    .gauge_style(Style::default().fg(color)).percent(100);
                f.render_widget(gauge, vertical[0]);
                f.render_widget(button, vertical[1]);
            }).unwrap();
            thread::sleep(Duration::from_millis(1));
        });

    let stdin = stdin();
    for c in stdin.keys() {
        match c.unwrap() {
            Key::Char('q') => break,
            _ => (),
        }
    };
    print!("{}[2J", 27 as char);
    Ok(())
}